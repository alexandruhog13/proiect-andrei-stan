const adminRoutes = require('./controllers/Admin.js');
const userRoutes = require('./controllers/User.js');
const authRoutes = require('./controllers/Auth.js');
const modelRoutes = require('./controllers/Models.js');

module.exports.bindRoutes = (app) => {
    app.use(`/api/${adminRoutes.rootPath}`, adminRoutes.router);
    app.use(`/api/${userRoutes.rootPath}`, userRoutes.router);
    app.use(`/api/${authRoutes.rootPath}`, authRoutes.router);
    app.use(`/${modelRoutes.rootPath}`, modelRoutes.router);
    
    app.use((err, req, res, next) => {
        if (!err.code || typeof err.code == "string") {
            err.code = 500;
        }
        res.status(err.code).json({
            error: err.message
        });
        next(err);
    })
};