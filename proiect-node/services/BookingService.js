const {
    query
} = require('../data/DatabaseConnector.js');
const {
    createError
} = require('../utils/ErrorUtils.js');

module.exports.create = async (userId, institutionId, serviceId, reservationTime) => {
    try {
        await query('INSERT INTO bookings (user_id, institution_id, service_id, reservation_time) VALUES ($1, $2, $3, $4)', [userId, institutionId, serviceId, reservationTime]);
        return {
            message: 'Successfully added'
        }
    } catch (err) {
        console.log(err);
        throw createError("The booking could not be inserted", 500);
    }
}

module.exports.getByUserId = async (userId) => {
    try {
        const {
            rows
        } = await query(`SELECT b.id, i.name as institution_name, i.email as institution_email, s.name as service_name, b.reservation_time
        FROM bookings b
        JOIN institutions i on b.institution_id = i.id
        JOIN services s on b.service_id = s.id
        WHERE b.user_id = $1`, [userId]);

        return {
            message: "Bookings for user",
            bookings: rows
        }
    } catch (err) {
        throw createError("Could not fetch the bookings", 500);
    }
}

module.exports.getAll = async () => {
    try {
        const {
            rows
        } = await query(`SELECT b.id, i.name as institution_name, i.email as institution_email, s.name as service_name, b.reservation_time, u.email as user_email, u.phone as user_phone
        FROM bookings b
        JOIN institutions i on b.institution_id = i.id
        JOIN services s on b.service_id = s.id
        JOIN users u on b.user_id = u.id`, []);

        return {
            message: "Bookings for admin",
            bookings: rows
        }
    } catch (err) {
        throw createError("Could not fetch the bookings", 500);
    }
}


module.exports.delete = async (id) => {
    try {
      await query('DELETE FROM bookings WHERE id = $1',
        [id]);
      return {
        message: 'Booking deleted successfully!'
      }
    } catch (err) {
      console.log(err);
      throw createError("The booking could not be deleted", 500);
    };
  };