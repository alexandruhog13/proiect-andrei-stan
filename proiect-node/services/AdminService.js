const {query} = require('../data/DatabaseConnector.js');
const {createError} = require('../utils/ErrorUtils.js');
const {hash, compare} = require('../security/PasswordEncoder.js');
const {generateToken} = require('../security/JwtTokenProvider.js');

module.exports.register = async (email, password) => {
    const hashedPassword = await hash(password);
    try {
        await query('insert into admins (email, password) values ($1, $2)', [email, hashedPassword])
        return {
            message: `Admin ${email} registrated`,
        }
    } catch (err) {
        console.log(err);
        if (err.code === '23505') {
            throw createError('An admin with this email already exists', 500);
        }
        throw createError('Could not create the admin', 500);
    }
}

module.exports.login = async (email, password) => {
    try {
        const {
            rows,
        } = await query('SELECT id, password FROM admins WHERE email = $1', [email]);
        if (rows.length === 1) {
            if (compare(password, rows[0].password)) {
                const token = await generateToken({
                    id: rows[0].id,
                    email: email,
                    scope: 'admin'
                });
                return {
                    message: 'Authentication succesful',
                    data: {
                        token: token,
                        isAdmin: true
                    }
                };
            } else {
                throw createError('Wrong email and password combination', 401);
            }
        } else {
            throw createError('No account for this user', 404);
        }
    } catch (err) {
        console.log(err);
        if (err.code === 401 || err.code === 404) {
            throw err;
        }
        throw createError('Could not retrieve the user', 500);
    };
}