const {
  query
} = require('../data/DatabaseConnector.js');
const {
  createError
} = require('../utils/ErrorUtils.js');

module.exports.insert = async (name, description, price, coin, institutionId) => {
  try {
    await query('INSERT INTO services (name, description, price, coin, institution_id) VALUES ($1, $2, $3, $4, $5)',
      [name, description, price, coin, institutionId]);
    return {
      message: 'Successfully added'
    }
  } catch (err) {
    console.log(err);
    throw createError("The service could not be inserted", 500);
  }
}

module.exports.getAll = async () => {
  try {
    const {
      rows,
    } = await query(`
        SELECT s.id, s.name, s.description, s.price, s.coin, 
        i.email as institution_email, i.name as institution_name
        FROM services s
        LEFT JOIN institutions i ON s.institution_id = i.id`,
      []);
    return {
      message: `All services with basic institution information`,
      data: {
        services: rows
      }
    }
  } catch (err) {
    throw createError('Could not retrieve the services', 500);
  }
};

module.exports.update = async (toBeUpdated) => {
  try {
    let str = 'UPDATE services SET ';
    const len = toBeUpdated.keys.length;
    if (toBeUpdated.keys.length > 1) {
      for (i = 0; i < len - 1; i++) {
        str += `${toBeUpdated.keys[i]} = '${toBeUpdated.values[i]}', `
      }
    }
    str += `${toBeUpdated.keys[len - 1]} = '${toBeUpdated.values[len - 1]}' WHERE id = $1 RETURNING *`
    const {
      rows
    } = await query(str, [toBeUpdated.id]);
    return {
      message: 'Service updated successfully!',
      data: rows[0]
    }
  } catch (err) {
    console.log(err);
    throw createError("The service could not be updated", 500);
  };
};


module.exports.delete = async (id) => {
  try {
    await query('DELETE FROM services WHERE id = $1',
      [id]);
    return {
      message: 'Service deleted successfully!'
    }
  } catch (err) {
    console.log(err);
    throw createError("The service could not be deleted", 500);
  };
};