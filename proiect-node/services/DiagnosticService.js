const {
    query
} = require('../data/DatabaseConnector.js');
const {
    createError
} = require('../utils/ErrorUtils.js');

module.exports.insert = async (name, description, pastTreatments, userId) => {
    try {
        await query('INSERT INTO diagnostics (name, description, past_treatments, user_id) VALUES ($1, $2, $3, $4)', [name, description, pastTreatments, userId]);
        return {
            message: 'Successfully added'
        }
    } catch (err) {
        throw createError("The diagnostic could not be inserted", 500);
    }
}