const {
    query
} = require('../data/DatabaseConnector.js');
const {
    createError
} = require('../utils/ErrorUtils.js');
const Mail = require('../mail/Mail.js');
const {
    hash,
    compare
} = require('../security/PasswordEncoder.js');
const uniqid = require('uniqid');
const {
    generateToken
} = require('../security/JwtTokenProvider.js');

module.exports.register = async (email, password, firstName, lastName, phone) => {
    const hashedPassword = await hash(password);
    const validationUrl = uniqid();
    try {
        await query('INSERT INTO users (email, password, firstname, lastname, phone, validationurl, validated) VALUES ($1, $2, $3, $4, $5, $6, $7)', [email, hashedPassword, firstName, lastName, phone, validationUrl, false])
        const mailClient = new Mail()
            .setTo(email)
            .setSubject('Code Verification')
            .setText(`Please verify your registration by entering this URL ${process.env.CLIENTROUTE}/validate/user/${validationUrl}`);
        console.log(await mailClient.sendMail());

        return {
            message: `Awaiting validation for ${email}`,
            data: {
                urlToClick: `${process.env.CLIENTROUTE}/validate/user/${validationUrl}`,
                urlToCall: `/auth/validate/user/${validationUrl}`
            }
        }
    } catch (err) {
        console.log(err);
        if (err.code === '23505') {
            throw createError('A user with this email already exists', 500);
        }
        throw createError('Could not create the user', 500);
    }
};
module.exports.login = async (email, password) => {
    try {
        const {
            rows,
        } = await query('SELECT id, password FROM users WHERE email = $1 AND validated = $2', [email, true]);
        if (rows.length === 1) {
            if (await compare(password, rows[0].password)) {
                const token = await generateToken({
                    id: rows[0].id,
                    email: email,
                    scope: 'user'
                });
                return {
                    message: 'Authentication succesful',
                    data: {
                        token: token,
                        isAdmin: false
                    }
                };
            } else {
                throw createError('Wrong email and password combination', 401);
            }
        } else {
            throw createError('No account for this user', 404);
        }
    } catch (err) {
        console.log(err);
        if (err.code === 401 || err.code === 404) {
            throw err;
        }
        throw createError('Could not retrieve the user', 500);
    };
}
module.exports.validate = async (validationUrl) => {
    try {
        const {
            rows,
        } = await query(`UPDATE users SET validated = true, validationurl = 'NULL' WHERE validated = $1 AND validationurl = $2 returning *`, [false, validationUrl]);
        return {
            message: `Validated ${rows[0].email} with success`,
        }
    } catch (err) {
        console.log(err);
        throw createError('Could not validate the user', 500);
    }
}
module.exports.getByEmail = async (email) => {
    try {
        const {
            rows,
        } = await query(`
        SELECT u.email, u.firstname, u.lastname, u.phone, d.description, d.name, d.past_treatments
        FROM users u
        LEFT JOIN diagnostics d ON u.id = d.user_id
        WHERE u.email = $1 AND u.validated = $2`,
            [email, true]);

        let userProfile = {
            email: rows[0].email,
            firstName: rows[0].firstname,
            lastName: rows[0].lastname,
            phone: rows[0].phone,
            diagnostics: []
        };

        rows.forEach(row => {
            userProfile.diagnostics.push({
                name: row.name,
                description: row.description,
                pastTreatments: row.past_treatments,
            });
        });

        return {
            message: `User ${email} profile`,
            data: {
                profile: userProfile
            }
        }
    } catch (err) {
        throw createError('Could not retrieve the user', 500);
    }
};

module.exports.forgotPasswordSendEmail = async (email) => {
    const passwordResetUrl = uniqid();
    try {
        const {rows} = await query(`UPDATE users SET resetpasswordurl = $1 WHERE email = $2 returning *`, [passwordResetUrl, email]);
        const mailClient = new Mail()
        .setTo(email)
        .setSubject('Password reset')
        .setText(`Please enter this URL to reset your password ${process.env.CLIENTROUTE}/password/reset/user/${passwordResetUrl}`);
        console.log(await mailClient.sendMail());
        return {
            message: `Awaiting password reset for ${email}`,
            data: {
                urlToClick: `${process.env.CLIENTROUTE}/password/reset/user/${passwordResetUrl}`,
                urlToCall: `/auth/password/reset/user/${passwordResetUrl}`
            }
        }
    } catch (err) {
        console.log(err);
        throw createError('Something terrible happened', 500);
    }
};

module.exports.resetPassword = async (passwordResetUrl, password) => {
    const hashedPassword = await hash(password);
    try {
        const {rows} = await query(`UPDATE users SET resetpasswordurl = 'NULL', password = $1 WHERE resetpasswordurl = $2 returning *`, [hashedPassword, passwordResetUrl]);
        return {
            message: 'Password reset successful',
            data: rows[0]
        }
    } catch (err) {
        console.log(err);
        throw createError('Could not reset the password', 500);
    }
}