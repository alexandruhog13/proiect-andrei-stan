const {
  query
} = require('../data/DatabaseConnector.js');
const {
  createError
} = require('../utils/ErrorUtils.js');
const Mail = require('../mail/Mail.js');

module.exports.insert = async (email, name, description, phone, lat, long, schedule, category) => {
  try {
    const {
      rows
    } = await query('INSERT INTO institutions (email, name, description, phone, lat, long, schedule, category) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *',
      [email, name, description, phone, lat, long, schedule, category]);
    return {
      message: `Successfully added institution ${rows[0].id}`,
      data: {
        id: rows[0].id,
      }
    }
  } catch (err) {
    console.log(err);
    throw createError("The institution could not be inserted", 500);
  }
}

module.exports.getByEmail = async (email) => {
  try {
    const {
      rows,
    } = await query(`
    SELECT i.id, i.email, i.name, i.description, i.phone, i.lat, i.long, i.schedule, i.category, 
    s.id as service_id, s.name as service_name, s.description as service_description, s.price as service_price, s.coin as service_coin
    FROM institutions i
    LEFT JOIN services s ON s.institution_id = i.id 
    WHERE i.email = $1`, [email]);

    let institution = {
      id: rows[0].id,
      name: rows[0].name,
      email: rows[0].email,
      description: rows[0].description,
      phone: rows[0].phone,
      lat: rows[0].lat,
      long: rows[0].long,
      schedule: rows[0].schedule,
      category: rows[0].category,
      services: []
    }

    rows.forEach((row) => {
      institution.services.push({
        serviceId: row.service_id,
        serviceName: row.service_name,
        serviceDescription: row.service_description,
        servicePrice: row.service_price,
        serviceCoin: row.service_coin
      });
    });

    return {
      message: `Institution details`,
      data: {
        institution: institution
      }
    }
  } catch (err) {
    console.log(err);
    throw createError('Could not retrieve the institution', 500);
  }
};

module.exports.getAll = async () => {
  try {
    const {
      rows,
    } = await query(`
          SELECT i.id, i.email, i.name, i.description, i.phone, i.lat, i.long, i.schedule, i.category, 
          s.id as service_id, s.name as service_name, s.description as service_description, s.price as service_price, s.coin as service_coin
          FROM institutions i
          LEFT JOIN services s ON s.institution_id = i.id
          ORDER BY i.id`,
      []);

    let institutionsArray = [];
    let name = rows[0].name;
    let index = 0;
    institutionsArray.push({
      id: rows[0].id,
      name: rows[0].name,
      email: rows[0].email,
      description: rows[0].description,
      phone: rows[0].phone,
      lat: rows[0].lat,
      long: rows[0].long,
      schedule: rows[0].schedule,
      category: rows[0].category,
      services: []
    });
    rows.forEach((row) => {
      if (row.name !== name) {
        name = row.name;
        index = index + 1;
        institutionsArray.push({
          id: row.id,
          name: row.name,
          email: row.email,
          description: row.description,
          phone: row.phone,
          lat: row.lat,
          long: row.long,
          schedule: row.schedule,
          category: row.category,
          services: []
        });
      }
      institutionsArray[index].services.push({
        serviceId: row.service_id,
        serviceName: row.service_name,
        serviceDescription: row.service_description,
        servicePrice: row.service_price,
        serviceCoin: row.service_coin
      });
    });

    return {
      message: `All institutions with service details`,
      data: {
        institutions: institutionsArray
      }
    }
  } catch (err) {
    console.log(err);
    throw createError('Could not retrieve the institutions', 500);
  }
};

module.exports.sendMailToInstitution = async (from, to, text) => {
  const mailClient = new Mail().setFrom(from).setTo(to).setSubject("Contact").setText(`Email from ${from}: 
  ${text}`);
  console.log(await mailClient.sendMail());
  return {
    message: "Email sent successfully"
  };
};

module.exports.update = async (toBeUpdated) => {
  try
   {
    let str = 'UPDATE institutions SET ';
    const len = toBeUpdated.keys.length;
    if (toBeUpdated.keys.length > 1){
      for ( i = 0; i < len - 1; i ++){
        str += `${toBeUpdated.keys[i]} = '${toBeUpdated.values[i]}', `
      }
    }
    str += `${toBeUpdated.keys[len - 1]} = '${toBeUpdated.values[len - 1]}' WHERE id = $1 RETURNING *`
    const {
      rows
    } = await query(str, [toBeUpdated.id]);
    return {
      message: 'Institution updated successfully!',
      data: rows
    }
  } catch (err) {
    console.log(err);
    throw createError("The institution could not be updated", 500);
  };
};

module.exports.delete = async (id) => {
  try {
    await query('DELETE FROM institutions WHERE id = $1',
      [id]);
    return {
      message: 'Institution deleted successfully!'
    }
  } catch (err) {
    console.log(err);
    throw createError("The institution could not be deleted", 500);
  };
};