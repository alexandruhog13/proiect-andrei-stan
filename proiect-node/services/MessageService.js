const {
  query
} = require('../data/DatabaseConnector.js');
const {
  createError
} = require('../utils/ErrorUtils.js');

module.exports.sendFromUser = async (email, text) => {
  try {
    const {
      rows
    } = await query('INSERT INTO messages (user_email, text, type) VALUES ($1, $2, $3) RETURNING *',
      [email, text, "from_user"]);
    return {
      message: `Successfully sent message ${rows[0].id}`,
      data: rows[0]
    }
  } catch (err) {
    console.log(err);
    throw createError("The message could not be sent", 500);
  }
};

module.exports.fetchForUser = async (email) => {
  try {
    const {
      rows
    } = await query('SELECT * FROM messages WHERE user_email = $1 AND visible_to_user = true AND type = $2 ORDER BY timestamp ASC',
      [email, "to_user"]);
    if (rows.length === 0) {
      return {
        message: `Messages from admin:`,
        data: []
      }
    }
    return {
      message: `Messages from admin:`,
      data: rows
    }
  } catch (err) {
    console.log(err);
    throw createError("The message could not be retreived", 500);
  }
};

module.exports.fetchSentByUser = async (email) => {
  try {
    const {
      rows
    } = await query('SELECT * FROM messages WHERE user_email = $1 AND type = $2 AND visible_to_user = true ORDER BY timestamp ASC',
      [email, "from_user"]);

    if (rows.length === 0) {
      return {
        message: `Messages sent by users:`,
        data: []
      }
    }
    return {
      message: `Messages sent by users:`,
      data: rows
    }
  } catch (err) {
    console.log(err);
    throw createError("The message could not be retreived", 500);
  }
};

module.exports.sendToUser = async (email, text) => {
  try {
    const {
      rows
    } = await query('INSERT INTO messages (user_email, text, type) VALUES ($1, $2, $3) RETURNING *',
      [email, text, "to_user"]);
    return {
      message: `Successfully sent message ${rows[0].id}`,
      data: rows[0]
    }
  } catch (err) {
    console.log(err);
    throw createError("The institution could not be inserted", 500);
  }
};

module.exports.fetchFromUsers = async () => {
  try {
    const {
      rows
    } = await query('SELECT * FROM messages WHERE type = $1 AND visible_to_admin = true ORDER BY user_email, timestamp',
      ["from_user"]);

    if (rows.length === 0) {
      return {
        message: `Messages sent by users:`,
        data: []
      }
    }
    let messagesArray = [];
    let userEmail = rows[0].user_email;
    let index = 0;
    messagesArray.push({
      userEmail: rows[0].user_email,
      messages: []
    });
    rows.forEach((row) => {
      if (row.user_email !== userEmail) {
        userEmail = row.user_email;
        index = index + 1;
        messagesArray.push({
          userEmail: row.user_email,
          messages: []
        });
      } 
      messagesArray[index].messages.push({
        messageId: row.id,
        messageText: row.text,
        messageTimestamp: row.timestamp,
        type: row.type
      });
    });
    return {
      message: `Messages from users:`,
      data: messagesArray
    }
  } catch (err) {
    console.log(err);
    throw createError("The messages could not have been fetched", 500);
  }
};

module.exports.fetchSentByAdmin = async () => {
  try {
    const {
      rows
    } = await query('SELECT * FROM messages WHERE type = $1 AND visible_to_admin = true ORDER BY user_email, timestamp',
      ["to_user"]);

    if (rows.length === 0) {
      return {
        message: `Messages sent to users:`,
        data: []
      }
    }
    let messagesArray = [];
    let userEmail = rows[0].user_email;
    let index = 0;
    messagesArray.push({
      userEmail: rows[0].user_email,
      messages: []
    });
    rows.forEach((row) => {
      if (row.user_email !== userEmail) {
        userEmail = row.user_email;
        index = index + 1;
        messagesArray.push({
          userEmail: row.user_email,
          messages: []
        });
      }
      messagesArray[index].messages.push({
        messageId: row.id,
        messageText: row.text,
        messageTimestamp: row.timestamp,
        type: row.type
      });
    });

    return {
      message: `Messages sent to users:`,
      data: messagesArray
    }
  } catch (err) {
    console.log(err);
    throw createError("The messages could not be fetched", 500);
  }
};

module.exports.deleteFromAdmin = async (id) => {
  try {
    const {
      rows
    } = await query('UPDATE messages SET visible_to_admin = false WHERE id = $1 RETURNING *', [id]);

    if (rows[0].visible_to_user === false) {
      await this.delete(id);
    }
    return {
      message: 'Message deleted successfully!'
    }
  } catch (err) {
    console.log(err);
    throw createError("The message could not be deleted", 500);
  }
}

module.exports.deleteFromUser = async (id) => {
  try {
    const {
      rows
    } = await query('UPDATE messages SET visible_to_user = false WHERE id = $1 RETURNING *', [id]);

    if (rows[0].visible_to_admin === false) {
      await this.delete(id);
    }
    return {
      message: 'Message deleted successfully!'
    }
  } catch (err) {
    console.log(err);
    throw createError("The message could not be deleted", 500);
  }
}

module.exports.delete = async (id) => {
  try {
    await query('DELETE FROM messages WHERE id = $1', [id]);
    return {
      message: 'Message deleted successfully!'
    }
  } catch (err) {
    console.log(err);
    throw createError("The message could not be deleted", 500);
  }
};