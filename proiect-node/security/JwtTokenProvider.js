const jwt = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");

const generalOptions = {
  issuer: process.env.ISSUER,
  expiresIn: process.env.EXPIRATIONTIME
};
const signOptions = {
  ...generalOptions,
  algorithm: process.env.ALGORITHM
};
const verifyOptions = {
  ...generalOptions,
  algorithm: [process.env.ALGORITHM]
};

const privateKey = fs.readFileSync(
  path.join(__dirname, "../secret.pem"),
  "utf8"
);
const publicKey = fs.readFileSync(
  path.join(__dirname, "../secret.pub.pem"),
  "utf8"
);

module.exports.generateToken = async data => {
  const token = await jwt.sign(data, privateKey, signOptions);
  return token; 
};

module.exports.verifyToken = async token => {
  let result;
  try {
    result = await jwt.verify(token, publicKey, verifyOptions);
    return result;
  } catch (err) {
    return false;
  }
};