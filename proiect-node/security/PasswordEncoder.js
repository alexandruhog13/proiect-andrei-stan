const bcrypt = require('bcryptjs');

module.exports.hash = async (plainTextPassword) => {
  const salt = await bcrypt.genSalt(Number.parseInt(process.env.SALT, 10));
  const hashedPassword = await bcrypt.hash(plainTextPassword, salt);
  return hashedPassword;
};

module.exports.compare = async (plainTextPassword, hashedPassword) => {
  const match = await bcrypt.compare(plainTextPassword, hashedPassword);
  return match;
};
