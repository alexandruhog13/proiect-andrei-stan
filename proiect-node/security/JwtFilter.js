const validator = require("validator");
const HttpStatus = require("http-status-codes");
const jwtTokenProvider = require("./JwtTokenProvider.js");
const {createError} = require("../utils/ErrorUtils.js");

module.exports.authorizeToken = async (req, res, next) => {
  if (!req.headers.authorization) {
    throw createError("Unauthorized!", HttpStatus.FORBIDDEN);
  }
  const token = req.headers.authorization.split(" ")[1];
  if (!validator.isJWT(token)) {
    throw createError("Wrong Token Provided", HttpStatus.BAD_REQUEST);
  }
  const decoded = await jwtTokenProvider.verifyToken(token);
  if (!decoded) {
    throw createError("Unauthorized!", HttpStatus.FORBIDDEN);
  }
  req.state = {
    token,
    decoded
  };
  next();
};

module.exports.authorizeAdmin = async (req, res, next) => {
  if (req.state.decoded.scope !== "admin") {
    throw createError("Unauthorized!", HttpStatus.FORBIDDEN);
  }
  next();
};

module.exports.authorizeUser = async (req, res, next) => {
  if (req.state.decoded.scope !== "user") {
    throw createError("Unauthorized!", HttpStatus.FORBIDDEN);
  }
  next();
};

module.exports.extendToken = async (req, res, next) => {
  if (req.state.decoded.rememberMe === true) {
    let now = Math.floor(new Date() / 1000);
    let exp = req.state.decoded.exp;
    if ((exp - now) / (60 * 60 * 24) <= 1) {
      // il pot reinnoi
      const oldDecoded = req.state.decoded;
      req.state.token = await jwtTokenProvider.generateToken({
        id: oldDecoded.id,
        scope: oldDecoded.scope,
        rememberMe: true
      });
    }
  }
  next();
};