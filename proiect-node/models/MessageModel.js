const query = 
`
CREATE TYPE message_type AS ENUM ('from_user', 'to_user');
CREATE TABLE messages(
    id serial NOT NULL,
    user_email character varying NOT NULL,
    text text NOT NULL,
    type message_type NOT NULL,
    timestamp timestamp with time zone NOT NULL default CURRENT_TIMESTAMP,
    visible_to_admin boolean NOT NULL default TRUE,
    visible_to_user boolean NOT NULL default TRUE,
    CONSTRAINT messages_pkey PRIMARY KEY (id)
)`;

module.exports = query;