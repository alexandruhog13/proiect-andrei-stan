const query = 
`
CREATE TABLE bookings(
    id serial NOT NULL,
    user_id integer NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    institution_id integer NOT NULL REFERENCES institutions(id) ON DELETE CASCADE,
    service_id integer NOT NULL REFERENCES services(id) ON DELETE CASCADE,
    reservation_time date NOT NULL,
    CONSTRAINT bookings_pkey PRIMARY KEY (id)
)`;

module.exports = query;