const query = 
`
CREATE TABLE services(
    id serial NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    price real NOT NULL,
    coin character(3) NOT NULL,
    institution_id integer NOT NULL REFERENCES institutions(id) ON DELETE CASCADE,   
    CONSTRAINT services_pkey PRIMARY KEY (id)
)`;

module.exports = query;