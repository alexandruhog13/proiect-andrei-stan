const query = 
`
CREATE TYPE categories AS ENUM ('wellness', 'health');
CREATE TABLE institutions(
    id serial NOT NULL,
    email character varying NOT NULL UNIQUE,
    name character varying NOT NULL,
    description character varying NOT NULL,
    lat real NOT NULL,
    long real NOT NULL,
    schedule character varying NOT NULL,
    category categories NOT NULL,
    phone character varying NOT NULL,
    CONSTRAINT institutions_pkey PRIMARY KEY (id)
)`;

module.exports = query;