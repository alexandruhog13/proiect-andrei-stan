const query = 
`
CREATE TABLE diagnostics(
    id serial NOT NULL,
    name character varying NOT NULL,
    past_treatments character varying NOT NULL,
    description character varying NOT NULL,
    user_id integer NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT diags_pkey PRIMARY KEY (id)
)`;

module.exports = query;