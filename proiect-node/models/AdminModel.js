const query = `
CREATE TABLE admins
(
    id serial NOT NULL,
    email character varying NOT NULL UNIQUE,
    password character varying NOT NULL,
    CONSTRAINT admins_pkey PRIMARY KEY (id)
)`;
module.exports = query;
