const query = `
CREATE TABLE users
(
    id serial NOT NULL,
    email character varying NOT NULL UNIQUE,
    password character varying NOT NULL,
    firstname character varying NOT NULL,
    lastname character varying NOT NULL,
    phone character varying NOT NULL UNIQUE,
    validationurl character varying,
    resetpasswordurl character varying,
    validated boolean DEFAULT false NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)`;
module.exports = query;
