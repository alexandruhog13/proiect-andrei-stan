module.exports.createError = (message, code) => {
    let error = new Error(message);
    error.code = code;
    return error;
}