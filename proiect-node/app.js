const express = require('express');
const app = express();
const cors = require('cors');
const morgan = require('morgan');
require('dotenv').config();
require('express-async-errors');
app.use(cors());
app.use(express.json());

app.use(morgan('combined'));
const {
    bindRoutes
} = require('./routes.js');
bindRoutes(app);
app.listen(process.env.PORT, (err) => {
    if (err) {
        console.error(err);
    } else {
        console.log(`app is listening on port ${process.env.PORT}`);
    }
});
