const {authorizeToken, authorizeAdmin, authorizeUser} = require('../security/JwtFilter.js');
const Router = require('express').Router();
const InstitutionService = require('../services/InstitutionService.js');
const ServicesService = require('../services/ServicesService.js');
const BookingService = require('../services/BookingService.js');
const MessagesService = require("../services/MessageService.js");

const HTTP_STATUS = require('http-status-codes');

/* INSTITUTIONS */
Router.post("/institution", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await InstitutionService.insert(
        req.body.email,
        req.body.name,
        req.body.description,
        req.body.phone,
        req.body.lat,
        req.body.long,
        req.body.schedule,
        req.body.category);
    res.status(HTTP_STATUS.CREATED).json(data);
});

Router.get("/institutions", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await InstitutionService.getAll();
    res.status(HTTP_STATUS.OK).json(data);
});

Router.delete("/institutions/:id", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await InstitutionService.delete(req.params.id);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.put("/institutions/:id", authorizeToken, authorizeAdmin, async (req, res, next) => {
      const toBeUpdated = {
        keys: [],
        values: [],
        id: req.params.id,
      };
      if (req.body.email) {
        toBeUpdated.keys.push('email');
        toBeUpdated.values.push(req.body.email);
      }
      if (req.body.name) {
        toBeUpdated.keys.push('name');
        toBeUpdated.values.push(req.body.name);
      }
      if (req.body.description) {
        toBeUpdated.keys.push('description');
        toBeUpdated.values.push(req.body.description);
      }
      if (req.body.phone) {
        toBeUpdated.keys.push('phone');
        toBeUpdated.values.push(req.body.phone);
      }
      if (req.body.lat) {
        toBeUpdated.keys.push('lat');
        toBeUpdated.values.push(req.body.lat);
      }
      if (req.body.long) {
        toBeUpdated.keys.push('long');
        toBeUpdated.values.push(req.body.long);
      }
      if (req.body.schedule) {
        toBeUpdated.keys.push('schedule');
        toBeUpdated.values.push(req.body.schedule);
      }
      if (req.body.category) {
        toBeUpdated.keys.push('category');
        toBeUpdated.values.push(req.body.category);
      }
    
    const data = await InstitutionService.update(toBeUpdated);
    res.status(HTTP_STATUS.OK).json(data);
});

/* SERVICES */
Router.post("/service", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await ServicesService.insert(
        req.body.name,
        req.body.description,
        req.body.price,
        req.body.coin,
        req.body.institutionId);
    res.status(HTTP_STATUS.CREATED).json(data);
});

Router.get("/services", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await ServicesService.getAll();
    res.status(HTTP_STATUS.OK).json(data);
});

Router.delete("/services/:id", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await ServicesService.delete(req.params.id);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.put("/services/:id", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const toBeUpdated = {
      keys: [],
      values: [],
      id: req.params.id,
    };
    if (req.body.name) {
      toBeUpdated.keys.push('name');
      toBeUpdated.values.push(req.body.name);
    }
    if (req.body.description) {
      toBeUpdated.keys.push('description');
      toBeUpdated.values.push(req.body.description);
    }
    if (req.body.price) {
      toBeUpdated.keys.push('price');
      toBeUpdated.values.push(req.body.price);
    }
    if (req.body.coin) {
      toBeUpdated.keys.push('coin');
      toBeUpdated.values.push(req.body.coin);
    }
  const data = await ServicesServices.update(toBeUpdated);
  res.status(HTTP_STATUS.OK).json(data);
});

/* BOOKINGS */
Router.get("/bookings", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await BookingService.getAll();
    res.status(HTTP_STATUS.OK).json(data);
});

Router.delete("/bookings/:id", authorizeToken, authorizeAdmin, async (req, res, next) => {
  const data = await BookingService.delete(req.params.id);
  res.status(HTTP_STATUS.OK).json(data);
});

/* MESSAGE */
Router.post("/message", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await MessagesService.sendToUser(req.body.email, req.body.text);
    res.status(HTTP_STATUS.OK).json(data);

});

Router.get("/message/fromUsers", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await MessagesService.fetchFromUsers();
    res.status(HTTP_STATUS.OK).json(data);
});

Router.get("/message/sentByMe", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await MessagesService.fetchSentByAdmin();
    res.status(HTTP_STATUS.OK).json(data);
});

Router.delete("/message/:id", authorizeToken, authorizeAdmin, async (req, res, next) => {
    const data = await MessagesService.deleteFromAdmin(req.params.id);
    res.status(HTTP_STATUS.OK).json(data);
});

module.exports = {
    rootPath: 'admin',
    router: Router
};
