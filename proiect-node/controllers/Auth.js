const Router = require('express').Router();
const AdminService = require('../services/AdminService.js');
const UserService = require('../services/UserService.js');
const HTTP_STATUS = require('http-status-codes');

Router.post('/admin/signup', async (req, res, next) => {
    const data = await AdminService.register(req.body.email, req.body.password);
    res.status(HTTP_STATUS.CREATED).json(data);
});

Router.post('/user/signup', async (req, res, next) => {
    const data = await UserService.register(req.body.email, req.body.password, req.body.firstName, req.body.lastName, req.body.phone);
    res.status(HTTP_STATUS.CREATED).json(data);
});

Router.post('/admin/login', async (req, res, next) => {
    const data = await AdminService.login(req.body.email, req.body.password);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.post('/user/login', async (req, res, next) => {
    const data = await UserService.login(req.body.email, req.body.password);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.post('/user/password/reset/generateUrl', async (req, res, next) => {
    const data = await UserService.forgotPasswordSendEmail(req.body.email);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.post('/password/reset/user/:passwordResetUrl', async (req, res, next) => {
    const data = await UserService.resetPassword(req.params.passwordResetUrl, req.body.password);
    res.status(HTTP_STATUS.OK).json(data);
})

Router.get('/validate/user/:validationUrl', async (req, res, next) => {
    const data = await UserService.validate(req.params.validationUrl);
    res.status(HTTP_STATUS.OK).json(data);
})

module.exports = {
    rootPath: 'auth',
    router: Router
}