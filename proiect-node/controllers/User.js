const Router = require('express').Router();
const UserService = require('../services/UserService.js');
const DiagnosticService = require('../services/DiagnosticService.js');
const InstitutionService = require('../services/InstitutionService.js');
const ServicesService = require("../services/ServicesService.js");
const BookingService = require("../services/BookingService.js");
const MessagesService = require("../services/MessageService.js");

const {authorizeToken, authorizeUser} = require('../security/JwtFilter.js');
const HTTP_STATUS = require('http-status-codes');

/* PROFILE */
Router.get('/profile', authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await UserService.getByEmail(req.state.decoded.email);
    res.status(HTTP_STATUS.OK).json(data);
})

/* DIAGNOSTIC */
Router.post('/diagnostic', authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await DiagnosticService.insert(req.body.name, req.body.description, req.body.pastTreatments, req.state.decoded.id);
    res.status(HTTP_STATUS.CREATED).json(data);
})

/* INSTITUTIONS */
Router.get("/institutions/all", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await InstitutionService.getAll();
    res.status(HTTP_STATUS.OK).json(data);
})

Router.post("/institutions/byEmail", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await InstitutionService.getByEmail(req.body.email);
    res.status(HTTP_STATUS.OK).json(data);
})

Router.post("/institutions/contact", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await InstitutionService.sendMailToInstitution(req.state.decoded.email, req.body.email, req.body.text);
    res.status(HTTP_STATUS.OK).json(data);
})

/* SERVICES */
Router.get("/services/all", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await ServicesService.getAll();
    res.status(HTTP_STATUS.OK).json(data);
})

/* BOOKINGS */
Router.post("/bookings", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await BookingService.create(req.state.decoded.id,
        req.body.institutionId,
        req.body.serviceId,
        req.body.reservationTime
        );
    res.status(HTTP_STATUS.CREATED).json(data);
});

Router.get("/bookings", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await BookingService.getByUserId(req.state.decoded.id);
    res.status(HTTP_STATUS.OK).json(data);
});

/* MESSAGE */
Router.post("/message", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await MessagesService.sendFromUser(req.state.decoded.email, req.body.text);
    res.status(HTTP_STATUS.OK).json(data);

});

Router.get("/message/fromAdmin", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await MessagesService.fetchForUser(req.state.decoded.email);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.get("/message/sentByMe", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await MessagesService.fetchSentByUser(req.state.decoded.email);
    res.status(HTTP_STATUS.OK).json(data);
});

Router.delete("/message/:id", authorizeToken, authorizeUser, async (req, res, next) => {
    const data = await MessagesService.deleteFromUser(req.params.id);
    res.status(HTTP_STATUS.OK).json(data);
});
module.exports = {
    rootPath: 'user',
    router: Router
};
