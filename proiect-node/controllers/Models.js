const Router = require('express').Router();
const Admin = require('../models/AdminModel.js');
const User = require('../models/UserModel.js');
const Institution = require('../models/InstitutionModel.js');
const Service = require('../models/ServiceModel.js');
const Diagnostic = require('../models/DiagnosticModel.js');
const Booking = require('../models/BookingModel.js');
const Message = require('../models/MessageModel.js');

const {query} = require('../data/DatabaseConnector.js');

Router.get('/createAdminTable', async (req, res, next) => {
    await query(Admin, []);
    res.status(200).json({
        message:"Admin table created!"
    });
});

Router.get('/createUserTable', async (req, res, next) => {
    await query(User, []);
    res.status(200).json({
        message:"User table created!"
    });
});

Router.get('/createDiagnosticTable', async (req, res, next) => {
    await query(Diagnostic, []);
    res.status(200).json({
        message:"Diagnostic table created!"
    });
});

Router.get('/createInstitutionTable', async (req, res, next) => {
    await query(Institution, []);
    res.status(200).json({
        message:"Institution table created!"
    });
});

Router.get('/createServiceTable', async (req, res, next) => {
    await query(Service, []);
    res.status(200).json({
        message:"Service table created!"
    });
});

Router.get('/createBookingTable', async (req, res, next) => {
    await query(Booking, []);
    res.status(200).json({
        message:"Booking table created!"
    });
});

Router.get('/createMessageTable', async (req, res, next) => {
    await query(Message, []);
    res.status(200).json({
        message:"Message table created!"
    });
});

module.exports = {
    rootPath: 'models',
    router: Router
};
