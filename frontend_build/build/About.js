import React from 'react';
import RegisterBox from './RegisterBox';
import LoginBox from './LoginBox';
import './Home.css';
import './App.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';
import { IoMdPerson } from 'react-icons/io';
import SkyLight from 'react-skylight';

class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoginOpen: true,
            isRegisterOpen: false
        };
        this.showLoginBox = this.showLoginBox.bind(this);
        this.showRegisterBox = this.showRegisterBox.bind(this);
    }
    showLoginBox() {
        this.setState({ isLoginOpen: true, isRegisterOpen: false });
    }

    showRegisterBox() {
        this.setState({ isRegisterOpen: true, isLoginOpen: false });
    }
    render() {
        const linkstyle = {
            marginRight: "3%",
            color: "#022445",
            flexDirection: 'row',
            cursor: 'pointer',
        };
        var myBigGreenDialog = {
            backgroundColor: '#ffffff',
            color: '#022445',
            width: '40%',
            height: '600px',
            marginTop: '-300px',
            marginLeft: '-20%',
        };
        var logostyle = {
            marginRight : "50%",
            color: "#022445",
            flexDirection: 'row',
            cursor: 'pointer',
        }
        return (
            <div className="container">
                <div className="header">
                        <ul className="links">
                            <Link style={logostyle} to='/'> <li><img src="./logo_file.png" className="logo"></img>
                            </li></Link>
                            <Link style={linkstyle} to='/about'><li>About Us</li></Link>
                            <Link style={linkstyle} to='/categories'><li>Categories</li></Link>
                            <li style={linkstyle} onClick={() => this.simpleDialog.show()}>Login <IoMdPerson style={{ verticalAlign: '-11%' }} /></li>
                            <SkyLight dialogStyles={myBigGreenDialog} hideOnOverlayClicked ref={ref => this.simpleDialog = ref}>
                                <div className="root-container">
                                    <div className="box-controller">
                                        <div
                                            className={"controller " + (this.state.isLoginOpen
                                                ? "selected-controller"
                                                : "")}
                                            onClick={this
                                                .showLoginBox
                                                .bind(this)}>
                                            Login
</div>
                                        <div
                                            className={"controller " + (this.state.isRegisterOpen
                                                ? "selected-controller"
                                                : "")}
                                            onClick={this
                                                .showRegisterBox
                                                .bind(this)}>
                                            Register
</div>
                                    </div>
                                    <div className="box-container">
                                        {this.state.isLoginOpen && <LoginBox />}
                                        {this.state.isRegisterOpen && <RegisterBox />}
                                    </div>
                                </div>
                            </SkyLight>
                        </ul>
                </div>
            </div >
        );
    }
}
export default About;