package com.example.demo.controller;

import com.example.demo.error.UserNotFoundException;
import com.example.demo.model.Admin;
import com.example.demo.model.User;
import com.example.demo.repository.AdminRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {


    @Autowired
    private AdminRepository repository;

    @GetMapping
    public List<Admin> findAll() {
        return repository.findAll();
    }

    @GetMapping("/{id}")
    public Admin findById(@PathVariable("id") Long id) {
        return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }
}
