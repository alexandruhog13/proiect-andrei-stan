package com.example.demo.controller;

import com.example.demo.model.Admin;
import com.example.demo.model.AuthenticationRequestAdmin;
import com.example.demo.model.AuthenticationRequestUser;
import com.example.demo.model.User;
import com.example.demo.repository.AdminRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.jsonwebtoken.JwtTokenProvider;
import com.example.demo.service.admin.AdminDetailsService;
import com.example.demo.service.user.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/admin/register")
    @ResponseStatus(HttpStatus.CREATED)
    Admin newAdmin(@RequestBody Admin admin) {
        return adminRepository.save(admin);
    }
    @PostMapping("/admin/login")
    public ResponseEntity loginAdmin (@RequestBody AuthenticationRequestAdmin data){
        try{
            Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword()));
            String token = jwtTokenProvider.createToken(auth, "admin");

            Map<String, Object> model = new HashMap<>();
            model.put("email", data.getUsername());
            model.put("token", token);
            return ok(model);
        } catch (AuthenticationException e){
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }

    @PostMapping("/user/register")
    @ResponseStatus(HttpStatus.CREATED)
    User newUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    @PostMapping("/user/login")
    public ResponseEntity loginUser (@RequestBody AuthenticationRequestUser data){
        try{
            Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(data.getEmail(), data.getPassword()));
            String token = jwtTokenProvider.createToken(auth, "user");

            Map<String, Object> model = new HashMap<>();
            model.put("email", data.getEmail());
            model.put("token", token);
            return ok(model);
        } catch (AuthenticationException e){
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }
}
