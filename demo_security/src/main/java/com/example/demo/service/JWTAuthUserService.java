package com.example.demo.service;

import com.example.demo.service.admin.AdminDetailsService;
import com.example.demo.service.admin.AdminPrincipal;
import com.example.demo.service.user.MyUserDetailsService;
import com.example.demo.service.user.MyUserPrincipial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JWTAuthUserService implements UserDetailsService {

    @Autowired
    private AdminDetailsService adminDetailsService;
    @Autowired
    private MyUserDetailsService userDetailsService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetails user = null;

        user = userDetailsService.loadUserByUsername(s);
        if (user == null) {
            user = adminDetailsService.loadUserByUsername(s);
        }
        if (user != null){
            return user;
        } else {
            throw new UsernameNotFoundException("User '" + s + "' not found");
        }
    }
}
