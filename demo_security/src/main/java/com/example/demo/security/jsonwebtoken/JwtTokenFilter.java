package com.example.demo.security.jsonwebtoken;

import com.example.demo.service.admin.AdminDetailsService;
import com.example.demo.service.user.MyUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private MyUserDetailsService userDetailsService;
    @Autowired
    private AdminDetailsService adminDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String token = jwtTokenProvider.resolveToken(httpServletRequest);
        if (token != null) {
            try {
                Jws<Claims> claims = jwtTokenProvider.validateToken(token);
                if (claims != null) {
                    String subject = claims.getBody().getSubject();
                    UserDetails userDetails = null;
                    if (claims.getBody().get("scope").equals("admin")){
                        userDetails = adminDetailsService.loadUserByUsername(subject);
                    } else {
                        userDetails = userDetailsService.loadUserByUsername(subject);
                    }
                    UsernamePasswordAuthenticationToken auth;
                    auth = new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
                    auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
