//package com.example.demo.security;
//
//import com.example.demo.service.admin.AdminDetailsService;
//import com.example.demo.service.user.MyUserDetailsService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class SecurityConfig_2 {
//
//    @Configuration
//    @Order(1)
//    public static class UserConfigurationAdapter extends WebSecurityConfigurerAdapter {
//
//        public UserConfigurationAdapter(){
//            super();
//        }
//        @Autowired
//        private MyUserDetailsService userDetailsService;
//
//        private SimpleUrlAuthenticationFailureHandler myFailureHandler = new SimpleUrlAuthenticationFailureHandler();
//        @Autowired
//        private MySuccessHandler mySuccessHandler;
//        @Autowired
//        private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
//
//        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//            auth.userDetailsService(userDetailsService)
//                    .passwordEncoder(encoder());
//        }
//
//        protected void configure(HttpSecurity http) throws Exception {
//            http
//                    .csrf().disable()
//                    .exceptionHandling()
//                    .authenticationEntryPoint(restAuthenticationEntryPoint)
//                    .and()
//
//                    .authorizeRequests()
//                    .antMatchers("/api/auth/user/login").permitAll()
//                    .antMatchers("/api/auth/user/register").permitAll()
//                    .antMatchers("/api/user/**").hasRole("USER")
//                    .and()
//
//                    .formLogin()
//                    .loginProcessingUrl("/api/auth/user/login")
//                    .successHandler(mySuccessHandler)
//                    .failureHandler(myFailureHandler)
//                    .and()
//                    .logout()
//                    .logoutUrl("/api/user/logout");
//        }
//    }
//
//    @Configuration
//    @Order(2)
//    public static class AdminConfigurationAdapter extends WebSecurityConfigurerAdapter {
//
//        public AdminConfigurationAdapter(){
//            super();
//        }
//        @Autowired
//        private AdminDetailsService adminDetailsService;
//
//        private SimpleUrlAuthenticationFailureHandler myFailureHandler = new SimpleUrlAuthenticationFailureHandler();
//        @Autowired
//        private MySuccessHandler mySuccessHandler;
//        @Autowired
//        private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
//
//        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//            auth.userDetailsService(adminDetailsService)
//                    .passwordEncoder(encoder());
//        }
//
//        protected void configure(HttpSecurity http) throws Exception {
//            http
//                    .csrf().disable()
//                    .exceptionHandling()
//                    .authenticationEntryPoint(restAuthenticationEntryPoint)
//                    .and()
//
//                    .authorizeRequests()
//                    .antMatchers("/api/auth/admin/login").permitAll()
//                    .antMatchers("/api/auth/admin/register").permitAll()
//                    .antMatchers("/api/admin/**").hasRole("ADMIN")
//                    .and()
//
//                    .formLogin()
//                    .loginProcessingUrl("/api/auth/admin/login")
//                    .successHandler(mySuccessHandler)
//                    .failureHandler(myFailureHandler)
//                    .and()
//                    .logout()
//                    .logoutUrl("/api/admin/logout");
//        }
//    }
//
//    @Bean
//    public static PasswordEncoder encoder() {
//        return new BCryptPasswordEncoder();
//    }
//}