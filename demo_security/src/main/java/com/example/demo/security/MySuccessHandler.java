package com.example.demo.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MySuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private RequestCache requestcache = new HttpSessionRequestCache();

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest req,
            HttpServletResponse res,
            Authentication auth) throws ServletException, IOException {

        SavedRequest savedRequest = requestcache.getRequest(req, res);

        if (savedRequest == null) {
            clearAuthenticationAttributes(req);
            return;
        }

        String targetUrlParam = getTargetUrlParameter();
        if (isAlwaysUseDefaultTargetUrl() || (targetUrlParam != null && StringUtils.hasText(req.getParameter(targetUrlParam)))) {
            requestcache.removeRequest(req, res);
            clearAuthenticationAttributes(req);
            return;
        }

        clearAuthenticationAttributes(req);
    }

    public void setRequestcache(RequestCache requestcache){
        this.requestcache = requestcache;
    }

}
