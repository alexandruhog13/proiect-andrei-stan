package com.example.demo.security.jsonwebtoken;

import com.example.demo.service.admin.AdminDetailsService;
import com.example.demo.service.admin.AdminPrincipal;
import com.example.demo.service.user.MyUserDetailsService;
import com.example.demo.service.user.MyUserPrincipial;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component
public class JwtTokenProvider {

    private String secretKey = "secretDiscret";
    private long validityInMilliSeconds= 360000;

    @Autowired
    MyUserDetailsService userDetailsService;
    @Autowired
    AdminDetailsService adminDetailsService;

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.replace("Bearer ", "");
        }
        return null;
    }

    public Jws<Claims> validateToken(String token) throws Exception {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            if (claims.getBody().getExpiration().before(new Date())) {
                return null;
            }
            return claims;
        } catch (JwtException | IllegalArgumentException e) {
            throw new Exception("Expired or invalid JWT token");
        }
    }

    public String createToken(Authentication auth, String type){
        UserDetails userDetails;
        if (type.equals("admin")){
             userDetails = (AdminPrincipal) auth.getPrincipal();
        } else {
            userDetails = (MyUserPrincipial) auth.getPrincipal();
        }
        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        claims.put("roles", userDetails.getAuthorities());
        claims.put("scope", type);

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliSeconds);
        return Jwts.builder()//
                .setClaims(claims)//
                .setIssuedAt(now)//
                .setExpiration(validity)//
                .signWith(SignatureAlgorithm.HS256, secretKey)//
                .compact();
    }
}
